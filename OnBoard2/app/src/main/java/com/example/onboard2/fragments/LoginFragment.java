package com.example.onboard2.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.onboard2.R;
import com.example.onboard2.activities.MainActivity;
import com.example.onboard2.activities.TermsOfUseActivity;
import com.example.onboard2.utils.NetworkHelper;
import com.example.onboard2.utils.SharedPreferenceHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class LoginFragment extends Fragment {

    public static final String TAG = LoginFragment.class.getSimpleName();

    @BindView(R.id.image_login)
    protected ImageView mImageLogin;

    @BindView(R.id.edit_email)
    protected EditText mEditTextEmail;

    @BindView(R.id.edit_password)
    protected EditText mEditTextPassword;

    @BindView(R.id.btn_login)
    protected Button mBtnLogin;

    @BindView(R.id.btn_terms_of_use)
    protected Button mBtnTermsOfUse;

    private Unbinder mUnbinder;
    private Boolean isOnline;

    public LoginFragment() {
    }

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public void onPause() {
        super.onPause();
        isOnline = false;
    }

    @Override
    public void onResume() {
        super.onResume();
        isOnline = true;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mEditTextEmail.setText("eve.holt@reqres.in");
        mEditTextPassword.setText("cityslicka");
        isOnline = true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }

    @OnClick(R.id.btn_login)
    protected void doLogin() {
        mBtnLogin.setEnabled(false);
        mBtnLogin.setText("Loading");
        final String email = mEditTextEmail.getText().toString().trim();
        final String password = mEditTextPassword.getText().toString(); // Password should not trim.
        //check email or password are blank or not
        if (email.equals("") || password.equals("")) {
            new MaterialDialog.Builder(getActivity())
                    .title("Error")
                    .content("Email or password cannot be empty.")
                    .positiveText("OK")
                    .show();
            mBtnLogin.setEnabled(true);
            mBtnLogin.setText("Login");
        } else {
            String url = "https://reqres.in/api/login";
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if (isOnline) {
                        //convert the response to the string
                        String token = "";
                        try {
                            JSONObject object = new JSONObject(response);
                            token = object.get("token").toString();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Toast.makeText(getActivity(), "Login success.", Toast.LENGTH_SHORT).show();
                        SharedPreferenceHelper.getInstance(getActivity())
                                .edit()
                                .putString("email", email)
                                .putString("token", token)
                                .apply();
                        startActivity(new Intent(getActivity(), MainActivity.class));
                        getActivity().finish();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    String message = "";
                    if (error instanceof NetworkError) {
                        message = "Your internet connection got problem.";
                    } else {
                        message = "Invalid Email";
                    }
                    new MaterialDialog.Builder(getActivity())
                            .title("Error")
                            .content(message)
                            .positiveText("OK")
                            .show();
                    mBtnLogin.setEnabled(true);
                    mBtnLogin.setText("Login");
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("email", email);
                    params.put("password", password);
                    return params;
                }
            };
            NetworkHelper.getRequestQueueInstance(getActivity()).add(stringRequest);
        }
    }

    @OnClick(R.id.btn_terms_of_use)
    public void doTermsOfUse() {
        startActivity(new Intent(getActivity(), TermsOfUseActivity.class));
    }
}