package com.example.onboard2.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.example.onboard2.R;
import com.example.onboard2.activities.FriendDetailActivity;
import com.example.onboard2.activities.LoginActivity;
import com.example.onboard2.adapters.FriendAdapter;
import com.example.onboard2.utils.SharedPreferenceHelper;
import com.example.onboard2.viewmodels.FriendViewModel;
import com.example.onboard2.viewmodels.FriendViewModelFactory;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class FriendListFragment extends Fragment implements FriendAdapter.CallBacks {

    public static final String TAG = FriendListFragment.class.getSimpleName();

    @BindView(R.id.recyclerview)
    protected RecyclerView mRecyclerView;

    @BindView(R.id.textview_no_data)
    protected TextView mNoData;

    private Unbinder mUnbinder;
    private FriendAdapter mAdapter;
    private FriendViewModel mViewModel;

    public FriendListFragment() {
    }

    public static FriendListFragment newInstance() {
        return new FriendListFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_fragment_friend_list, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.menu_logout) {
            // TODO show confirm dialog before continue.
            new MaterialDialog.Builder(getActivity())
                    .title("Logout")
                    .content("Are you sure ?")
                    .positiveText("Yes")
                    .negativeText("No")
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            dialog.dismiss();
                        }
                    })
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            // Logout user.
                            // Clear all user's data.
                            SharedPreferenceHelper.getInstance(getActivity())
                                    .edit()
                                    .clear()
                                    .apply();
                            // Go to login page.
                            startActivity(new Intent(getActivity(), LoginActivity.class));
                            getActivity().finish();
                        }
                    })
                    .show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().setTitle("ACL Training");
        mAdapter = new FriendAdapter();
        mAdapter.setCallBack(this);
        mAdapter.setData(null);
        mAdapter.notifyDataSetChanged();
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
        //the label which display "No data"
        mViewModel = new ViewModelProvider(this, new FriendViewModelFactory(getActivity().getApplication())).get(FriendViewModel.class);
        mViewModel.getFriendAdapterListLiveData().observe(getViewLifecycleOwner(), new Observer<List<FriendAdapter.DataHolder>>() {
            @Override
            public void onChanged(List<FriendAdapter.DataHolder> dataHolders) {

                mAdapter.setData(dataHolders);
                mAdapter.notifyDataSetChanged();
                if (dataHolders.size() > 0) {
                    mNoData.setVisibility(View.GONE);
                    mRecyclerView.setVisibility(View.VISIBLE);
                } else {
                    mNoData.setVisibility(View.VISIBLE);
                    mRecyclerView.setVisibility(View.GONE);
                }
            }
        });
        mViewModel.getError().observe(getViewLifecycleOwner(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean b) {
                if (b) {

                    new MaterialDialog.Builder(getActivity())
                            .title("Error")
                            .content("Your internet connection got problems")
                            .positiveText("OK")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    mViewModel.handleError();
                                    dialog.dismiss();
                                }
                            })
                            .show();
                }
            }
        });
        mViewModel.loadFriendAdapterList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_friend_list, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }

    @Override
    public void onListItemClicked(int id) {
        Intent intent = new Intent(getActivity(), FriendDetailActivity.class);
        intent.putExtra("id", id);
        startActivity(intent);
    }
}