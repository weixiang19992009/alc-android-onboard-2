package com.example.onboard2.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.example.onboard2.R;
import com.example.onboard2.adapters.FriendAdapter;
import com.example.onboard2.utils.NetworkHelper;
import com.example.onboard2.viewmodels.FriendViewModel;
import com.example.onboard2.viewmodels.FriendViewModelFactory;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class FriendDetailFragment extends Fragment {

    public static final String TAG = FriendDetailFragment.class.getSimpleName();
    private Unbinder mUnbinder;

    @BindView(R.id.textview_no_data)
    protected TextView mNoData;

    @BindView(R.id.textview_email)
    protected TextView mTextViewEmail;

    @BindView(R.id.textview_last_name)
    protected TextView mTextViewLastName;

    @BindView(R.id.textview_first_name)
    protected TextView mTextViewFirstName;

    @BindView(R.id.image_friend_detail)
    protected ImageView mImageFriendDetail;

    private FriendViewModel mViewModel;

    public FriendDetailFragment() {
    }

    public static FriendDetailFragment newInstance() {
        return new FriendDetailFragment();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().setTitle("ACL Training");
        //get the passing value from friend list fragment
        Bundle args = getActivity().getIntent().getExtras();
        int id = args.getInt("id");
        mViewModel = new ViewModelProvider(this, new FriendViewModelFactory(getActivity().getApplication())).get(FriendViewModel.class);
        mViewModel.getFriendLiveData().observe(getViewLifecycleOwner(), new Observer<FriendAdapter.DataHolder>() {
            @Override
            public void onChanged(FriendAdapter.DataHolder dataHolder) {
                mNoData.setVisibility(View.GONE);
                mTextViewEmail.setText(dataHolder.email);
                mTextViewLastName.setText(dataHolder.lastName);
                mTextViewFirstName.setText(dataHolder.firstName);
                //make the round image
                Glide.with(getActivity()).load(dataHolder.avatar).asBitmap().centerCrop().into(new BitmapImageViewTarget(mImageFriendDetail) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        mImageFriendDetail.setImageDrawable(circularBitmapDrawable);
                    }
                });
            }
        });
        mViewModel.getError().observe(getViewLifecycleOwner(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean b) {
                if (b) {
                    mTextViewEmail.setText("");
                    mTextViewLastName.setText("");
                    mTextViewFirstName.setText("");
                    mImageFriendDetail.setImageResource(0);
                    new MaterialDialog.Builder(getActivity())
                            .title("Error")
                            .content("Your internet connection got problems")
                            .positiveText("OK")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    mViewModel.handleError();
                                }
                            })
                            .show();
                }
            }
        });
        mViewModel.loadFriend(id);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_friend_detail, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }
}