package com.example.onboard2.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.onboard2.adapters.FriendAdapter;
import com.example.onboard2.repositories.FriendRepository;

import java.util.List;

public class FriendViewModel extends AndroidViewModel {
    private FriendRepository mFriendRepository;

    public FriendViewModel(@NonNull Application application) {
        super(application);
        mFriendRepository = FriendRepository.getInstance();
    }

    public LiveData<List<FriendAdapter.DataHolder>> getFriendAdapterListLiveData() {
        return mFriendRepository.getFriendAdapterListLiveData();
    }

    public LiveData<FriendAdapter.DataHolder> getFriendLiveData(){
        return mFriendRepository.getFriendLiveData();
    }

    public LiveData<Boolean> getError(){
        return mFriendRepository.getError();
    }

    public void handleError(){
        mFriendRepository.getError().setValue(false);
    }

    public void loadFriend(int id){
        mFriendRepository.loadFriend(getApplication(),id);
    }

    public void loadFriendAdapterList() {
        mFriendRepository.loadFriendAdapterList(getApplication());
    }
}
