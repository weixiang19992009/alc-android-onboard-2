package com.example.onboard2.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.onboard2.R;
import com.example.onboard2.utils.SharedPreferenceHelper;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Check user is logged in.
        if (!SharedPreferenceHelper.getInstance(this).contains("token")) {
            // User is not login yet.
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        } else {
            // User is logged in.Start the Friend list activity
            startActivity(new Intent(this, FriendListActivity.class));
            finish();
        }

    }
}