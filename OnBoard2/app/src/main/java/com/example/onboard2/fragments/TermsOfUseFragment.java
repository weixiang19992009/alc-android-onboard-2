package com.example.onboard2.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.onboard2.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class TermsOfUseFragment extends Fragment {

    public static final String TAG = TermsOfUseFragment.class.getSimpleName();
    private Unbinder mUnbinder;

    @BindView(R.id.webView)
    protected WebView mWebView;

    public TermsOfUseFragment() {
    }

    public static TermsOfUseFragment newInstance() {
        return new TermsOfUseFragment();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().setTitle("Terms Of Use");
        mWebView.loadUrl("https://www.dummies.com/terms-of-use/");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_terms_of_use, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }
}