package com.example.onboard2.viewmodels;


import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class FriendViewModelFactory implements ViewModelProvider.Factory {

    private Application mApplication;

    public FriendViewModelFactory(Application application) {
        mApplication = application;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new FriendViewModel(mApplication);
    }
}