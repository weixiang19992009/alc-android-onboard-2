package com.example.onboard2.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.onboard2.R;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FriendAdapter extends RecyclerView.Adapter<FriendAdapter.ViewHolder> {

    public static class DataHolder {
        public int id;
        public String email;

        @SerializedName("first_name")
        public String firstName;

        @SerializedName("last_name")
        public String lastName;
        public String avatar;

        public DataHolder(int id, String email, String firstName, String lastName, String avatar) {
            this.id = id;
            this.email = email;
            this.firstName = firstName;
            this.lastName = lastName;
            this.avatar = avatar;
        }
    }

    private List<DataHolder> mData = new ArrayList<>();
    private CallBacks mCallBack;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_friend, parent, false));
    }

    public void setCallBack(CallBacks mCallBack) {
        this.mCallBack = mCallBack;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.mTextViewEmail.setText(mData.get(position).email);
        holder.mTextViewFirstName.setText(mData.get(position).firstName);
        holder.mTextViewLastName.setText(mData.get(position).lastName);
        Glide.with(holder.mImageFriendList.getContext()).load(mData.get(position).avatar).into(holder.mImageFriendList);
        holder.mRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallBack.onListItemClicked(mData.get(position).id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setData(List<DataHolder> data) {
        if (data == null) {
            mData = new ArrayList<>();
        } else {
            mData = data;
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image_friend_list)
        public ImageView mImageFriendList;

        @BindView(R.id.lblEmail)
        public TextView mTextViewEmail;

        @BindView(R.id.lblFirstName)
        public TextView mTextViewFirstName;

        @BindView(R.id.lblLastName)
        public TextView mTextViewLastName;

        @BindView(R.id.linearlayout_root)
        public LinearLayout mRoot;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface CallBacks {
        void onListItemClicked(int id);
    }
}
