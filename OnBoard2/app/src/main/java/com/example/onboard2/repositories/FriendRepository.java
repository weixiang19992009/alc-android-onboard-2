package com.example.onboard2.repositories;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;

import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.lifecycle.MutableLiveData;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.example.onboard2.adapters.FriendAdapter;
import com.example.onboard2.utils.NetworkHelper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.List;

public class FriendRepository {
    private static FriendRepository mInstance;
    private MutableLiveData<List<FriendAdapter.DataHolder>> mFriendAdapterListLiveData = new MutableLiveData<>();
    private MutableLiveData<FriendAdapter.DataHolder> mFriendLiveData = new MutableLiveData<>();
    private MutableLiveData<Boolean> mNetworkError = new MutableLiveData<>();

    public static FriendRepository getInstance() {
        if (mInstance == null) {
            synchronized (FriendRepository.class) {
                mInstance = new FriendRepository();
            }
        }
        return mInstance;
    }

    public FriendRepository() {
    }

    public MutableLiveData<Boolean> getError() {
        return mNetworkError;
    }

    public MutableLiveData<List<FriendAdapter.DataHolder>> getFriendAdapterListLiveData() {
        return mFriendAdapterListLiveData;
    }

    public MutableLiveData<FriendAdapter.DataHolder> getFriendLiveData() {
        return mFriendLiveData;
    }

    public void loadFriendAdapterList(Context context) {
        //request the friend list
        StringRequest request = new StringRequest("https://reqres.in/api/users?page=1", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //convert response to the DataHolder list
                List<FriendAdapter.DataHolder> dataHolders;
                JSONArray array = null;
                Type type = new TypeToken<List<FriendAdapter.DataHolder>>() {
                }.getType();
                try {
                    JSONObject object = new JSONObject(response);
                    array = object.getJSONArray("data");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                dataHolders = new Gson().fromJson(array.toString(), type);
                mFriendAdapterListLiveData.setValue(dataHolders);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mNetworkError.setValue(true);
            }
        });
        NetworkHelper.getRequestQueueInstance(context).add(request);
    }

    public void loadFriend(Context context, int id) {
        //request the particular friend based on the id
        StringRequest request = new StringRequest("https://reqres.in/api/users/" + id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //convert response to the DataHolder object
                FriendAdapter.DataHolder dataHolder;
                JSONObject object = null;
                String result = "";
                try {
                    object = new JSONObject(response);
                    result = object.get("data").toString();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                dataHolder = new Gson().fromJson(result, FriendAdapter.DataHolder.class);
                mFriendLiveData.setValue(dataHolder);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mNetworkError.setValue(true);
            }
        });
        NetworkHelper.getRequestQueueInstance(context).add(request);
    }
}
